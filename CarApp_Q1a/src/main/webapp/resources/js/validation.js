

function getFeatures(){
	
	$('#result').empty();
	
   
    	
	  $.ajax({
			url: "getfeatures?name="+$('#car').val(),
	   		type: 'POST',
			beforeSend : function(){
				ajaxindicatorstart('Fetching Results.. please wait..');
			},
			success : function(jsonData){
				
				if(jsonData!=undefined){
					$('#result').append(' Speed : '+jsonData["speed"].toFixed(2)+ "<br> Color : "+jsonData["color"]+"<br> Price : "+jsonData["price"]);
				}else 	if(jsonData["message"]!=undefined){
					$('#result').append(jsonData["message"]);
				}
			},
			complete : function(){
				ajaxindicatorstop();
				
			},
			error : function(data){
				ajaxindicatorstop();
			}
		} );
	

}

 
