package com.sar.service;

import org.springframework.stereotype.Service;

import com.sar.impl.EcoSport;
import com.sar.impl.Endeavour;
import com.sar.impl.Figo;
import com.sar.impl.Mondeo;
import com.sar.intf.Car;


public interface CarService {
	
	public Car getCar(String name);

}
