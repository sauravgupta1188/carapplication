package com.sar.service;

import org.springframework.stereotype.Service;

import com.sar.impl.EcoSport;
import com.sar.impl.Endeavour;
import com.sar.impl.Figo;
import com.sar.impl.Mondeo;
import com.sar.intf.Car;

@Service
public class CarServiceImpl implements CarService{

public Car getCar(String name){
		
		Car car=null;
		
		if(name.equalsIgnoreCase("endeavour")){
			car=new Endeavour();
		}else if(name.equalsIgnoreCase("ecosport")){
			car=new EcoSport();
		}else if(name.equalsIgnoreCase("figo")){
			car=new Figo();
		}else if(name.equalsIgnoreCase("mondeo")){
			car=new Mondeo();
		}
		
		return car;
	}

}
