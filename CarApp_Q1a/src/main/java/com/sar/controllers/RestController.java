package com.sar.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.sar.bean.ErrorResponse;
import com.sar.exception.CarException;
import com.sar.intf.Car;
import com.sar.service.CarService;

@org.springframework.web.bind.annotation.RestController

public class RestController {

	@Autowired
	CarService carservice;
	
	@RequestMapping(value = "/getfeatures", method = RequestMethod.POST)
	public ResponseEntity<Map<String,Object>> getCarFeatures(
			@RequestParam(value = "name") final String name) throws CarException {
		
		Map<String,Object> rmap=new HashMap<String, Object>();
		Car car=carservice.getCar(name);
		
		if(car!=null){
			rmap=car.getFeatures();
		}
		
		if ( name.isEmpty() || car==null) {
			throw new CarException("Invalid Car requested");
		}

		return new ResponseEntity<Map<String,Object>>(rmap, HttpStatus.OK);
	}

	@ExceptionHandler(CarException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}
}