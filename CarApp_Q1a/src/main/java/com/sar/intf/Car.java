package com.sar.intf;
import java.util.Map;

public interface Car {
	
	
	public String getName();
	
	public double getSpeed();
	
	public String getColor();
	
	public double getPrice();
	
	public Map<String,Object> getFeatures();
	
	
}
