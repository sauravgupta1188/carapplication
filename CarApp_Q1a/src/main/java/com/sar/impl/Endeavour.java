package com.sar.impl;

import java.util.HashMap;
import java.util.Map;

import com.sar.intf.Car;

public class Endeavour implements Car{
	
	
	public String getName(){
	
		return "endeavour";
	}
	
	public double getSpeed(){
		return 100;
	}
	
	public String getColor(){
		return "white";
	}
	
	public double getPrice(){
		return 11000;
	}
	
	public Map<String,Object> getFeatures(){
		
		Map<String,Object> rmap=new HashMap<String, Object>();
		rmap.put("name", getName());
		rmap.put("speed", getSpeed());
		rmap.put("color", getColor());
		rmap.put("price", getPrice());
		
		return rmap;
	}


}
