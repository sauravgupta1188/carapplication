package com.sar.impl;

import java.util.HashMap;
import java.util.Map;

import com.sar.intf.Car;

public class Mondeo implements Car{
	
	
	public String getName(){
	
		return "mondeo";
	}
	
	public double getSpeed(){
		return 80;
	}
	
	public String getColor(){
		return "red";
	}
	
	public double getPrice(){
		return 7500;
	}
	
	public Map<String,Object> getFeatures(){
		
		Map<String,Object> rmap=new HashMap<String, Object>();
		rmap.put("name", getName());
		rmap.put("speed", getSpeed());
		rmap.put("color", getColor());
		rmap.put("price", getPrice());
		
		return rmap;
	}


}
