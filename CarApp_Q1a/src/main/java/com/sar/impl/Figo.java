package com.sar.impl;

import java.util.HashMap;
import java.util.Map;

import com.sar.intf.Car;

public class Figo implements Car{
	
	
	public String getName(){
	
		return "figo";
	}
	
	public double getSpeed(){
		return 120;
	}
	
	public String getColor(){
		return "yellow";
	}
	
	public double getPrice(){
		return 9000;
	}
	
	public Map<String,Object> getFeatures(){
		
		Map<String,Object> rmap=new HashMap<String, Object>();
		rmap.put("name", getName());
		rmap.put("speed", getSpeed());
		rmap.put("color", getColor());
		rmap.put("price", getPrice());
		
		return rmap;
	}


}
