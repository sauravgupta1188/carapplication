package com.sar.impl;

import java.util.HashMap;
import java.util.Map;

import com.sar.intf.Car;

public class EcoSport implements Car{
	
	
	public String getName(){
	
		return "ecosport";
	}
	
	public double getSpeed(){
		return 90;
	}
	
	public String getColor(){
		return "white";
	}
	
	public double getPrice(){
		return 10000;
	}
	
	public Map<String,Object> getFeatures(){
		
		Map<String,Object> rmap=new HashMap<String, Object>();
		rmap.put("name", getName());
		rmap.put("speed", getSpeed());
		rmap.put("color", getColor());
		rmap.put("price", getPrice());
		
		return rmap;
	}

}
